/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 00:06:09 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/20 23:27:59 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "calculator.h"
#include "ft_opp.h"
#include <stdlib.h>

int		check_op(char **av)
{
	int i;
	int res;

	i = 0;
	res = 0;
	while (i < 6)
	{
		if (av[2][0] == *g_opptab[i].c)
			res = 1;
		i++;
	}
	if (res == 0)
	{
		write(1, &"error : only [ - + * / % ] are accepted.\n", 41);
		return (0);
	}
	return (1);
}

int		check(int ac, char **av)
{
	if (ac != 4)
		return (0);
	if (!(check_op(av)))
		return (0);
	if (av[2][0] == '/' && ft_atoi(av[3]) == 0)
	{
		write(1, &"Stop : division by zero\n", 24);
		return (0);
	}
	else if (av[2][0] == '%' && ft_atoi(av[3]) == 0)
	{
		write(1, &"Stop : modulo by zero\n", 22);
		return (0);
	}
	return (1);
}

int		display(char *a, char *b, int (*f)(int, int))
{
	int res;

	res = 0;
	res = f(ft_atoi(a), ft_atoi(b));
	return (res);
}

int		main(int argc, char **argv)
{
	int			res;
	int			i;

	i = 0;
	res = 0;
	if (!(check(argc, argv)))
		return (0);
	if (argc == 4)
	{
		while (i < 6)
		{
			if (argv[2][0] == *g_opptab[i].c && argv[2][1] == '\0')
				res = display(argv[1], argv[3], *g_opptab[i].func);
			i++;
		}
		ft_putnbr(res);
		ft_putchar('\n');
	}
}
