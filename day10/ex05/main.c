/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 16:46:46 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/19 18:27:26 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int ft_is_sort(int *tab, int length, int(*f)(int, int));

int ft_getlen(int n, int n1)
{
	if (n > n1)
		return 1;
	else if(n < n1)
		return -1;
	else 
		return 0;
}

int main(int argc, char **argv)
{
	int mas;
	int i;
	int *arr;

	while (i < 4)
	{
		arr[i] = i;
		i++;
	}
	mas = 0;
	mas = ft_is_sort(arr, 4, &ft_getlen);
	printf("Result %d", mas);

	return (0);
}
