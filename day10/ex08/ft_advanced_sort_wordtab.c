/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_advanced_sort_wordtab.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 18:10:24 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/20 23:18:42 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_getlen(char **tab)
{
	int i;

	i = 0;
	while (tab[i])
		i++;
	return (i);
}

void	ft_advanced_sort_wordtab(char **tab, int (*cmp)(char *, char *))
{
	int		i;
	int		s;
	int		argc;
	char	*temp;

	argc = ft_getlen(tab);
	s = 0;
	while (s < argc)
	{
		i = 0;
		while (i < argc)
		{
			if (cmp(tab[s], tab[i]) < 0)
			{
				temp = tab[s];
				tab[s] = tab[i];
				tab[i] = temp;
			}
			i++;
		}
		s++;
	}
}
