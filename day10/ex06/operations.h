/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 12:47:00 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/20 13:45:06 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERATIONS_H

# define OPERATIONS_H

int		sum(int a, int b)
{
	return (a + b);
}

int		multiply(int a, int b)
{
	return (a * b);
}

int		substract(int a, int b)
{
	return (a - b);
}

int		divide(int a, int b)
{
	return (a / b);
}

int		mod(int a, int b)
{
	return (a % b);
}

#endif
