/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculator.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 19:12:32 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/20 23:15:21 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CALCULATOR_H

# define CALCULATOR_H

# include <unistd.h>
# include "operations.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_atoi(char *str)
{
	int num;
	int s;
	int i;

	i = 0;
	s = 1;
	num = 0;
	while (str[i] == ' ' || str[i] == '\t' || str[i] == '\n' ||
			str[i] == '\f' || str[i] == '\r' || str[i] == '\v')
		i++;
	if (str[i] == '-')
		s = -1;
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		num = num * 10 + (str[i] - '0');
		i++;
	}
	return (num * s);
}

int		ft_putbignbr(void)
{
	ft_putchar('-');
	ft_putchar('2');
	ft_putchar('1');
	ft_putchar('4');
	ft_putchar('7');
	ft_putchar('4');
	ft_putchar('8');
	ft_putchar('3');
	ft_putchar('6');
	ft_putchar('4');
	ft_putchar('8');
	return (0);
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
		ft_putbignbr();
	else if (nb > -2147483648 && nb <= 2147483647)
	{
		if (nb < 0)
		{
			ft_putchar('-');
			ft_putnbr(nb * -1);
			return ;
		}
		else if (nb < 10)
		{
			ft_putchar(nb + '0');
			return ;
		}
		else
		{
			ft_putnbr(nb / 10);
			ft_putchar(nb % 10 + '0');
		}
	}
	else
		return ;
}

#endif
