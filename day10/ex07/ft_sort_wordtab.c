/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_wordtab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 13:32:24 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/20 15:23:22 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0')
	{
		i++;
	}
	return (s1[i] - s2[i]);
}

int		ft_getlen(char **tab)
{
	int i;

	i = 0;
	while (tab[i])
		i++;
	return (i);
}

void	ft_sort_wordtab(char **tab)
{
	int		i;
	int		s;
	int		argc;
	char	*temp;

	argc = ft_getlen(tab);
	s = 0;
	while (s < argc)
	{
		i = 0;
		while (i < argc)
		{
			if (ft_strcmp(tab[s], tab[i]) < 0)
			{
				temp = tab[s];
				tab[s] = tab[i];
				tab[i] = temp;
			}
			i++;
		}
		s++;
	}
}
