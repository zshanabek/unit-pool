/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 16:46:46 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/19 16:51:57 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int *ft_map(int *tab, int length, int(*f)(int));
int ft_multiply(int a)
{
	a = a*2;
	return a;
}
int main()
{
	int *arr;
	int i;
	int *mas;
	while(i < 10)
	{
		arr[i] = i;
		i++;
	}

	mas = ft_map(arr, 10, &ft_multiply);
	i = 0;
	while(i < 10)
    {
		printf("%d", mas[i]);
		i++;
    }
	return (0);
}
