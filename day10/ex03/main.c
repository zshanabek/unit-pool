/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 16:46:46 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/19 17:16:00 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int ft_any(char **tab, int(*f)(char*));
int ft_getlen(char *arr)
{
	int i;

	i = 0;
	while(arr[i])
		i++;
	if (i == 5)
		return 1;
	return 0;
}
int main(int argc, char **argv)
{
	int mas;
	
	mas = 0;
	mas = ft_any(argv, &ft_getlen);
	printf("Result %d", mas);

	return (0);
}
