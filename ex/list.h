/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 19:15:30 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/21 19:17:15 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
#define LIST_H

typedef struct s_list t_list;

struct s_list
{
	char *str;
	t_list *next;
};

t_list	*add_link(t_list *list, char *str);
void print_list(t_list *list);

#endif
