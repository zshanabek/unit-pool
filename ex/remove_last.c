/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   remove_last.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 20:25:49 by zshanabe          #+#    #+#             */
/*   Updated: 2018/03/01 20:40:27 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

typedef struct node {
    void *data;
    struct node * next;
} t_list;

void    print_list(t_list *list)
{
    while(list)
    {
        printf("%s\n", list->data);
        list = list->next;
    }
}

t_list *add_link(t_list *list, void *data)
{
    t_list *tmp;
    
    tmp = malloc(sizeof(t_list));
    if (tmp)
    {
        tmp->data = data;
        tmp->next = list;
    }
    return (tmp);
}

int pop(t_list *head) {
    int retval = -1;
    t_list *next_node = NULL;

    if (head == NULL) {
        return -1;
    }

    next_node = (head)->next;
    retval = (head)->data;
    free(head);
    head = next_node;

    return retval;
}

int main(void)
{
	t_list *list;
	list = NULL;

	list = add_link(list, "tata");
	list = add_link(list, "toto");
	list = add_link(list, "tutu");
	pop(list);
	print_list(list);
    
	return (0);
}
