/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 19:10:27 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/21 19:27:02 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void ft_putstr(char *c);

void	print_list(t_list *list)
{
	while(list)
	{
		ft_putstr(list->str);
		list = list->next;
	}
}
