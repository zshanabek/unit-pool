/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 23:00:33 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/17 23:00:35 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"
#include <stdio.h>

int     main(int argc, char **argv)
{
    int i;
    int j;

    i = 0;
    struct s_stock_par *par = ft_param_to_tab(argc, argv);
    while (par[i].str)
    {
        printf("%s\n",par[i].str);
        printf("%d\n",par[i].size_param);
        j = 0;
        while (par[i].tab[j])
        {
            printf("%s\n", par[i].tab[j]);
            j++;
        }
        i++;
    }
}
