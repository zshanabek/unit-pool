/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex00.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 15:24:19 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/17 22:13:49 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str, int i)
{
	int len;

	len = 0;
	while (str[i] > 32 && str[i] != '\0')
	{
		len++;
		i++;
	}
	return (len);
}

int		ft_calculate_words(char *str)
{
	int words;
	int i;

	words = 0;
	i = 1;
	if (str[0] > 32)
		words++;
	while (str[i])
	{
		if (str[i - 1] <= 32 && str[i] > 32)
			words++;
		i++;
	}
	return (words);
}

int		ft_strfill(char *str, char *temp, int i)
{
	int h;

	h = 0;
	while (str[i] > 32)
	{
		temp[h] = str[i];
		i++;
		h++;
	}
	temp[h] = '\0';
	return (i);
}

char	**ft_split_whitespaces(char *str)
{
	char	**arr;
	int		i;
	int		g;

	g = -1;
	i = 0;
	arr = (char**)malloc(sizeof(char*) * (ft_calculate_words(str) + 1));
	if (arr == NULL)
		return (0);
	while (++g < ft_calculate_words(str))
	{
		while (str[i] <= 32 && str[i] != '\0')
			i++;
		arr[g] = malloc(sizeof(char*) * ft_strlen(str, i));
		if (arr[g] == NULL)
			return (0);
		i = ft_strfill(str, arr[g], i);
		arr[g] = arr[g];
	}
	arr[g] = NULL;
	return (arr);
}
