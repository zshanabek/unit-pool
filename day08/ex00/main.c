/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 16:29:26 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/17 16:42:12 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char    **ft_split_whitespaces(char *str);

int main()
{
	char c[] = "\t   Hello my lovely world! \n";
	char **s = ft_split_whitespaces(c);
	int i = 0;
	while (s[i])
	{
		printf("%s\n", s[i]);
		i++;
	}
}
