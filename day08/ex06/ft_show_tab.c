/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 23:31:23 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/17 23:33:53 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"

struct s_stock_par	*ft_param_to_tab(int ac, char **av);

void				ft_putchar(char c);

void				ft_putstr(char *str)
{
	int l;

	l = 0;
	while (str[l])
	{
		ft_putchar(str[l]);
		i++;
	}
}

void				ft_putnbr(int nb)
{
	if (nb < 0)
	{
		nb *= -1;
		ft_putchar('-');
	}
	else if (nb < 10)
	{
		ft_putchar(nb + '0');
		return ;
	}
	ft_putnbr(nb / 10);
	ft_putchar((nb % 10) + '0');
}

void				ft_show_tab(struct s_stock_par *par)
{
	int k;
	int l;

	k = 0;
	while (par[k])
	{
		ft_putstr(par[k].str);
		ft_putchar('\n');
		ft_putstr(par[k].size_param);
		ft_putchar('\n');
		l = 0;
		while (par[k].tab[l])
		{
			ft_putstr(par[k].tab[l]);
			ft_putchar('\n');
			l++;
		}
		k++;
	}
}
