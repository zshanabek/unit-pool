/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_bits.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 18:32:14 by zshanabe          #+#    #+#             */
/*   Updated: 2018/03/01 18:41:46 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

unsigned char swap_bits(unsigned char octet)
{
	return ((octet >> 4 & 0x0F) | (0xF0 & octet << 4 ));
}

int main()
{
	printf("res: %d\n", swap_bits(1));
}

//DEC| BIN
//16 | 00010000
//1  | 00000001
