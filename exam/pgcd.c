/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pgcd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 19:32:40 by zshanabe          #+#    #+#             */
/*   Updated: 2018/03/01 20:11:43 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int min(int a, int b)
{
	if (a > b)
		return a;
	else
		return b;
}

int pgcd(int a, int b)
{
	int div;
	int i;

	div = 1;
	i = 1;
	while (i < min(a, b))
	{
		if (a % i == 0 && b % i == 0)
			div = i;
		i++;
	}
	return (div);
}

int main(int ac, char **av)
{
	printf("res: %d", pgcd(atoi(av[1]), atoi(av[2])));
}
