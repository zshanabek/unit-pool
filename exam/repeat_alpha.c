/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeat_alpha.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 16:04:07 by zshanabe          #+#    #+#             */
/*   Updated: 2018/03/01 16:31:16 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int main(int argc, char **argv)
{
	int i;
	int position;
	int j;

	i = 0;

	if (argc == 1)
	{
		write(1, "\n", 1);
		return (0);
	}
	while (argv[1][i])
	{
		if (argv[1][i] >= 'a' && argv[1][i] <= 'z')
		{
			position = argv[1][i] - 'a' + 1;
			j = 0;
			while (j < position)
			{
				write(1, &argv[1][i], 1);
				j++;
			}
		}	
		else if (argv[1][i] >= 'A' && argv[1][i] <= 'Z') 
			position = argv[1][i] - 'A' + 1;
		i++;
	}
	write(1, "\n", 1);
}
