/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 18:13:01 by zshanabe          #+#    #+#             */
/*   Updated: 2018/03/01 19:00:06 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		get_num(char c)
{
	int n;

	if (c >= '0' && c <= '9')
		n = c - '0';
	else if (c >= 'A' && c <= 'Z')
		n = c - 'A' + 10;
	else if (c >= 'a' && c <= 'z')
		n = c - 'a' + 10;
	else
		n = -1;
	return (n);
}

int		atoi_base(char *str, int base)
{
	int i;
	int res;
	int num;

	res = 1;
	i = 0;
	while (str[i] <= 32)
		i++;
	if (str[i] == '-')
		res = -1;
	while (str[i] == '-' && str[i] == '+')
		i++;
	while (get_num(str[i]) >= 0 && get_num(str[i]) <= base)
	{
		num = num * base + get_num(str[i]);
		i++;
	}
	return (num*res);
}
int		main(int argc, char **argv)
{
	printf("res: %d", atoi_base(argv[1], atoi(argv[2])));
	return (0);
}
