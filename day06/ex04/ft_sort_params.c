/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 13:34:51 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/14 21:36:24 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0')
	{
		i++;
	}
	return (s1[i] - s2[i]);
}

void	ft_print_args(int argc, char **argv)
{
	int i;
	int j;

	i = 1;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j] != '\0')
		{
			ft_putchar(argv[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

int		main(int argc, char **argv)
{
	int			i;
	int			s;
	char		*temp;

	s = 1;
	while (s < argc)
	{
		i = 1;
		while (i < argc)
		{
			if (ft_strcmp(argv[s], argv[i]) > 0)
			{
				temp = argv[s];
				argv[s] = argv[i];
				argv[i] = temp;
			}
			i++;
		}
		s++;
	}
	ft_print_args(argc, argv);
}
