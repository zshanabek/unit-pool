#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *ft_strdup(char *src);

int main() {

	char name[] = "JonasTigr";
	char *n;

	n = ft_strdup(name);
	printf("Name = %s\n", n);
}

