/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 21:40:48 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/15 17:18:41 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int *nums;
	int len;
	int i;

	if (min >= max)
		return (0);
	len = max - min;
	nums = malloc(len);
	i = 0;
	while (i < len)
	{
		nums[i] = min;
		min++;
		i++;
	}
	return (nums);
}
