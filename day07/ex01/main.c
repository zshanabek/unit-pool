#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *ft_range(int min, int max);

int main() {
	int max;
	int min;
	int *nums;
	int i;

	i = 0;
	min = 8;
	max = 15;
	nums = ft_range(min, max);

	if( nums == NULL ) {
		fprintf(stderr, "Error - unable to allocate required memory\n");
	} else {
		while (nums[i] != '\0')
		{
			printf("Number: %d\n", nums[i]);
			i++;
		}
	}
}

