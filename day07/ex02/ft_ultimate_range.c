/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 13:49:26 by zshanabe          #+#    #+#             */
/*   Updated: 2018/02/15 17:24:16 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int len;
	int i;
	int *nums;

	if (min >= max)
	{
		*range = 0;
		return (0);
	}
	len = max - min;
	nums = malloc(sizeof(*nums) * len);
	i = 0;
	while (i < len)
	{
		nums[i] = min + i;
		i++;
	}
	*range = nums;
	return (i);
}
