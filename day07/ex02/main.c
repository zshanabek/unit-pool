#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int ft_ultimate_range(int **range, int min, int max);

int main() {
	int min;
	int max;
	int len;
	int *range;
	int **range1;

	range1 = &range;
	min = 4;
	max = 10;
	
	len = ft_ultimate_range(range1, min, max);

	printf("Result: %d\n", len);
	int i;
	
	i = 0;
	while (*range1[i] != '\0') 
	{
		printf("Number: %d\n", *range1[i]);
		i++;
	}
}
